package com.luv2code.springboot.kcrestaurant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeycloakRestaurantApplication {

    public static void main(String[] args) {
        SpringApplication.run(KeycloakRestaurantApplication.class, args);
    }

}
