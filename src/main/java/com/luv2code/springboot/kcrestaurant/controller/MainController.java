package com.luv2code.springboot.kcrestaurant.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    @GetMapping("/")
    public String home(){
        return "home";
    }

    @GetMapping("/employees")
    @PreAuthorize("hasRole('manager')")
    public String employees(){
        return "employees";
    }

    @GetMapping("/customers")
    public String customers(){
        return "customers";
    }
}
